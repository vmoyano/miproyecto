package hello;

public class Arrays {

	public static void imprimir(int[] a) {
		System.out.print("[ ");
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + " ");
		}
		System.out.println("]");
	}

	public static double promedio(int[] a) {
		double suma = 0.0;
		for (int i = 0; i < a.length; i++) {
			suma += a[i];
		}
		return suma / a.length;
	}

	// maximo de arreglo no vacío
	public static int maximo(int[] a) {
		int max = a[0];
		for (int i = 0; i < a.length; i++) {
			if (a[i] > max) {
				max = a[i];
			}
		}
		return max;
	}

	public static void main(String[] args) {
//		int[] arr = new int[3];
//		arr[0] = 5;
//		arr[1] = 7;
//		arr[2] = 10;
//
//		System.out.println(arr);
//		imprimir(arr);
//
//		int[] b = { 2, 3, 5 };
//		imprimir(b);
//		System.out.println(promedio(b));
//		System.out.println(maximo(b));

		int[] a = { 2, 5 };
		int[] b = { 2, 2, 2, 5 };
//		System.out.println(estaContenido(a, b));
	}





}
