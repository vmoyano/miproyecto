package hello;

import java.util.Scanner;

public class Hello {

	public static double promediar(int a, int b) {
		double suma = a + b;
		return suma / 2;
	}

	public static void saludar(String nombre) {
		System.out.println("Hola, " + nombre);
	}

	public static void main(String[] args) {
		System.out.println("Hola mundo!");

		int m = 3;
		boolean b = true;
		String nombre = "Messi";
		float promedio = 6;
		double temperatura = 37.1;

		if (temperatura > 30 || promedio > 7) {
			System.out.println("Hola, " + nombre);
		} else {
			System.out.println("Todo ok.");
		}

		// Conectores logicos
		// && y
		// || o
		// ! (negación)
		// == iguales
		// != distintos

		int n = 3;
		while (n <= 5) {
			System.out.println(n);
			n++;
		}

		// n++ o n = n+1 o ++n
		// n-- o n = n-1 o --n

		// for i in range (0,5)
		for (int i = 0; i < 5; i++) {
			System.out.println("i: " + i);

		}

		//Funciones
		saludar("Alberto");
		System.out.println(promediar(7, 6));

		//Scanner
		Scanner sc = new Scanner(System.in);
		System.out.println("Ingrese su nombre: ");
		String s = sc.nextLine();
		System.out.println("Ingrese un número: ");
		int a = sc.nextInt();
		System.out.println("Hola " + s);
		System.out.println("El número ingresado es: " + a);

	}

}
